
#include "Ship.h"


Ship::Ship()
{
	// Sets the position of the ship to 0,0
	SetPosition(0, 0);
	// Sets the collision radius of the ship
	SetCollisionRadius(10);

	// The ship movement speed
	m_speed = 300;
	// Amount of times the ship can be hit before deactivating
	m_maxHitPoints = 3;

	// If True, the ship cannot be destroyed
	m_isInvulnurable = false;

	Initialize();
}

void Ship::Update(const GameTime *pGameTime)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Update(pGameTime);
	}

	GameObject::Update(pGameTime);
}

void Ship::Hit(const float damage)
{
	// checks if ship is invincible
	if (!m_isInvulnurable)
	{
		// subtracts from ships health
		m_hitPoints -= damage;

		// if health is below 0, destroy
		if (m_hitPoints <= 0)
		{
			GameObject::Deactivate();
		}
	}
}

void Ship::Initialize()
{
	m_hitPoints = m_maxHitPoints;
}

void Ship::FireWeapons(TriggerType type)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Fire(type);
	}
}

void Ship::AttachWeapon(Weapon *pWeapon, Vector2 position)
{
	pWeapon->SetGameObject(this);
	pWeapon->SetOffset(position);
	m_weapons.push_back(pWeapon);
}