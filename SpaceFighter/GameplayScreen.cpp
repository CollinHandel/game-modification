
#include "GameplayScreen.h"
#include "Level.h"
#include "Level01.h"
#include "Level02.h"

GameplayScreen::GameplayScreen(const int levelIndex)
{
	m_pLevel = nullptr;
	m_pTexture = nullptr;
	switch (levelIndex)
	{
	case 0: m_pLevel = new Level01(); break;
	case 1: m_pLevel = new Level02(); break;
	}


	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);

	Show();
}

void GameplayScreen::LoadContent(ResourceManager *pResourceManager)
{
	// Logo
	m_pTexture = pResourceManager->Load<Texture>("Textures\\space.jpg");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150;

	m_pLevel->LoadContent(pResourceManager);
}

void GameplayScreen::HandleInput(const InputState *pInput)
{
	m_pLevel->HandleInput(pInput);
}

void GameplayScreen::Update(const GameTime *pGameTime)
{
	m_pLevel->Update(pGameTime);
}

void GameplayScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();

	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter(), Vector2((float)3, (float)3));

	pSpriteBatch->End();
	m_pLevel->Draw(pSpriteBatch);
}
